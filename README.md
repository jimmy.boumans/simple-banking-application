# Simple Banking Application
This beginner-friendly project shows how to make a simple banking application that acts as a checking account. It utilizes print statements, if/else statements, strings, ints, methods, loops, and more. 

Using these techniques, the bank program created allows the user to perform multiple different functions:

* Check your balance
* Make a deposit
* Make a withdrawal
* View the previous transaction
* Calculate Interest
* Exit the application

## Dependencies 
* JUnit 5.4
