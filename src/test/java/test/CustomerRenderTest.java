package test;

import com.dev.perso.object.Customer;
import com.dev.perso.render.CustomerRender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerRenderTest {

    private static final float CURRENT_ACCOUNT = 500.00f;
    private static final float DEPOSIT = 200.00f;
    private static final String DEPOSIT_STR = "200";
    private static final float WITHDRAWAL = 200.00f;
    private static final String WITHDRAWAL_STR = "200";

    private Customer customer;
    private CustomerRender customerRender = new CustomerRender();

    @BeforeEach
    void setUpCustomer() {
        customer = new Customer();
        customer.setCustomerAccount(CURRENT_ACCOUNT);
    }

    @Test
    @DisplayName("Customer Render - Check balance")
    void shouldReturnCustomerBalanceRender() {
        assertEquals("Your current balance is : " + CURRENT_ACCOUNT + "\n\r", customerRender.getCustomerBalanceRender(customer), "Customer Render : Check balance failed");
    }

    @Test
    @DisplayName("Customer Render - Make a deposit")
    void shouldReturnCustomerRenderDepositAcknowledgement() {
        assertEquals("Your new deposit of " + DEPOSIT + " have been registered.\n\r", customerRender.makeADepositRender(customer, DEPOSIT), "Customer Render : Make a deposit failed");
    }

    @Test
    @DisplayName("Customer Render - Ask deposit amount")
    void shouldReturnCustomerRenderDepositAmountWhenItOccur() {
        String input = DEPOSIT_STR;
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        assertEquals(DEPOSIT, customerRender.askAmountRender());
    }

    @Test
    @DisplayName("Customer Render - Make a withdrawal")
    void makeWithdrawalRender() {
        assertEquals("Your new withdrawal of " + WITHDRAWAL + " have been registered.\n\r", customerRender.makeAWithdrawalRender(customer, WITHDRAWAL), "Customer Render : Make a withdrawal failed");
    }

    @Test
    @DisplayName("Customer Render - Ask withdrawal amount")
    void shouldReturnCustomerRenderWithdrawalAmountWhenItOccur() {
        String input = WITHDRAWAL_STR;
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        assertEquals(WITHDRAWAL, customerRender.askAmountRender());
    }
}
