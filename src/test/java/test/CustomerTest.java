package test;

import com.dev.perso.object.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerTest {

    private static final float CURRENT_ACCOUNT = 500.00f;
    private static final float DEPOSIT = 200.00f;
    private static final float WITHDRAWAL = 200.00f;

    private Customer customer;

    @BeforeEach
    void setUpCustomer() {
        customer = new Customer();
        customer.setCustomerAccount(CURRENT_ACCOUNT);
    }

    @Test
    @DisplayName("Customer - Check balance")
    void shouldReturnCustomerBalance() {
        assertEquals(CURRENT_ACCOUNT, customer.getCurrentBalance(), "Customer : Check balance failed");
    }


    @Test
    @DisplayName("Customer - Make a deposit")
    void shouldIncreaseCustomerBalanceAfterDeposit() {
        customer.makeDeposit(customer, DEPOSIT);
        assertEquals(CURRENT_ACCOUNT + DEPOSIT, customer.getCurrentBalance(), "Customer : Make a deposit failed");
    }

    @Test
    @DisplayName("Customer - Make a withdrawal")
    void shouldReduceCustomerBalanceAfterWithdrawal() {
        customer.makeAWithdrawal(customer, WITHDRAWAL);
        assertEquals(CURRENT_ACCOUNT - WITHDRAWAL, customer.getCurrentBalance(), "Customer : Make a withdrawal failed");
    }


}