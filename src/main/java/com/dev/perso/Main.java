package com.dev.perso;

import com.dev.perso.object.Customer;
import com.dev.perso.render.CustomerRender;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        boolean exit = false;
        int userChoice;
        Scanner sc = new Scanner(System.in);
        Customer customer = new Customer();
        CustomerRender customerRender = new CustomerRender();

        System.out.println("Welcome in you simple banking application:");

        while (!exit) {
            System.out.println("What would you like to do ? ");
            System.out.println("1 - Print 'hello' to the world!");
            System.out.println("2 - Check your balance");
            System.out.println("3 - Make a deposit");
            System.out.println("4 - Make a withdrawal");
            System.out.println("5 - Exit");

            userChoice = sc.nextInt();

            switch (userChoice) {
                case 1:
                    System.out.println("Hello world ! \n\r");
                    break;
                case 2:
                    System.out.println(customerRender.getCustomerBalanceRender(customer));
                    break;
                case 3:
                    float deposit = customerRender.askAmountRender();
                    System.out.println(customerRender.makeADepositRender(customer, deposit));
                    break;
                case 4:
                    float withdrawal = customerRender.askAmountRender();
                    System.out.println(customerRender.makeAWithdrawalRender(customer, withdrawal));
                    break;
                case 5:
                    exit = true;
                    break;
                default:
                    System.out.println("Waiting...");
                    break;
            }
        }

    }
}
