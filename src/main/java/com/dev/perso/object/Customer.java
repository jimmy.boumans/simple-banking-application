package com.dev.perso.object;

public class Customer extends Account {
    private Account account = new Account();

    public Customer() {
    }

    public Account getAccount() {
        return account;
    }

    public void setCustomerAccount(float balance) {
        this.account.setBalance(balance);
    }

    public float getCurrentBalance() {
        return account.getBalance();
    }

    public void makeDeposit(Customer customer, float deposit) {
        account.setBalance(customer.getCurrentBalance() + deposit);
    }

    public void makeAWithdrawal(Customer customer, float withdrawal) {
        account.setBalance(customer.getCurrentBalance() - withdrawal);
    }
}
