package com.dev.perso.object;

public class Account {
    private float balance;

    public Account(int id, float balance) {
        this.balance = balance;
    }

    public Account() {
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float newBalance) {
        this.balance = newBalance;
    }
}
