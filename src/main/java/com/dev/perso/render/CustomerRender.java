package com.dev.perso.render;

import com.dev.perso.object.Customer;

import java.util.Scanner;

public class CustomerRender extends Customer {

    public CustomerRender() {
        super();
    }

    public String getCustomerBalanceRender(Customer customer) {
        return "Your current balance is : " + customer.getCurrentBalance() + "\n\r";
    }

    public String makeADepositRender(Customer customer, float deposit) {
        customer.makeDeposit(customer, deposit);
        return "Your new deposit of " + deposit + " have been registered." + "\n\r";
    }

    public float askAmountRender() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, select an amount : ");
        return sc.nextFloat();
    }

    public String makeAWithdrawalRender(Customer customer, float withdrawal) {
        customer.makeAWithdrawal(customer, withdrawal);
        return "Your new withdrawal of " + withdrawal + " have been registered." + "\n\r";
    }

}
